# -*- coding: utf-8 -*-
# sms-gateway, A web interface to send SMS.
# Copyright (C) 2021 IKUS Software inc.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import base64
import logging
import urllib.parse

import cherrypy
import jinja2
import requests
import pkg_resources

from sms_gateway.i18n import gettext
from sms_gateway.i18n import gettext as _
from sms_gateway.i18n import ngettext

logger = logging.getLogger(__name__)


class Root(object):
    """
    Root entry point exposed using cherrypy.
    """

    env = jinja2.Environment(
        loader=jinja2.PackageLoader('sms_gateway'),
        auto_reload=True,
        autoescape=True,
        extensions=[
            'jinja2.ext.i18n',
            'jinja2.ext.with_',
            'jinja2.ext.autoescape',
        ]
    )

    def __init__(self, cfg):
        self.cfg = cfg
        self.env.install_gettext_callables(gettext, ngettext, newstyle=True)

    def render_template(self, name, **kwargs):
        tmpl = self.env.get_template('index.html')
        return tmpl.render(
            header_name=self.cfg.header_name,
            footer_url=self.cfg.footer_url,
            footer_name=self.cfg.footer_name,
            **kwargs)

    @cherrypy.expose
    @cherrypy.tools.i18n(mo_dir=pkg_resources.resource_filename('sms_gateway', 'locales'), default='en_US', domain='messages')
    def index(self):
        return self.render_template('index.html')

    @cherrypy.expose
    def sender(self):
        """
        Return the user's signature to be used for sending message.
        """
        # Basic authentication might be in place to let retreive the value
        auth_header = cherrypy.request.headers.get('authorization')
        if auth_header:
            print(auth_header)
            scheme, unused, value = auth_header.partition(' ')
            if scheme == 'Basic':
                login = base64.b64decode(value.encode('ascii')).decode('utf8').partition(':')[0]
                return login
        return self.cfg.websms_sender

    @cherrypy.expose
    @cherrypy.tools.json_out()
    @cherrypy.tools.json_in()
    def send(self):
        """
        Send SMS messatge using WebSMS Restful API.
        """
        data = cherrypy.request.json
        assert isinstance(data, dict), "input data expect to be a json object"
        assert isinstance(data['message'], str), "message must be a string"
        assert isinstance(data['recipients'], list) and data['recipients'], "recipients must be a list"
        print(data)
        ret = requests.post('https://api.websms.com/rest/smsmessaging/simple', params={
            'access_token': self.cfg.websms_token,
            'recipientAddressList': data['recipients'],
            'messageContent': data['message'],
            'test': self.cfg.websms_test,
            'maxSmsPerMessage': 0,
            'senderAddress': self.sender(),
            'senderAddressType': 'alphanumeric',
            'sendAsFlashSms': 'false'})
        if ret.status_code != 200:
            logger.warning('smsmessaging return an error: %s', ret.text)
            raise cherrypy.HTTPError(
                status=400,
                message=_('Fail to send message to WebSMS. HTTP Status Code: %s', ret.status_code))
        data = urllib.parse.parse_qs(ret.text)
        status_code = data.get('statusCode', None)
        if status_code != ['2000']:
            status_message = data.get('statusMessage', None)
            raise cherrypy.HTTPError(
                status=400,
                message=_('Fail to send message to WebSMS. Status Code: %s, Status Message: %s' % (status_code, status_message)))
        return _('Successfully sent your message. %s SMS have been delivered.', 5)
