
# -*- coding: utf-8 -*-
# sms-gateway, A web interface to send SMS.
# Copyright (C) 2021 IKUS Software inc.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import cherrypy.test.helper

from sms_gateway.app import Root
from sms_gateway.config import parse_args


class TestApp(cherrypy.test.helper.CPWebCase):

    @classmethod
    def setup_server(cls):
        cfg = parse_args(['--websms-token', 'token'])
        app = Root(cfg)
        cherrypy.tree.mount(app)

    def test_index(self):
        # Given the application is started
        # When making a query to index page
        self.getPage('/')
        # Then an html page is returned
        self.assertStatus(200)
        self.assertInBody('<body>')

    def test_sender_without_auth(self):
        # Given the application is started
        # When making a query to sender page
        self.getPage('/sender')
        # Then a default sender name is returned
        self.assertStatus(200)
        self.assertBody('SMS-Gateway')

    def test_sender_with_basic_auth(self):
        # Given the application is started
        # When making a query to sender page with valid Authorization header
        self.getPage('/sender', headers=[('Authorization', 'Basic aWt1czA2MDp0ZXN0MTIz')])
        # Then a default sender name is returned
        self.assertStatus(200)
        self.assertBody('ikus060')
